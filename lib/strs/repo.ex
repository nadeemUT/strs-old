defmodule Strs.Repo do
  use Ecto.Repo,
    otp_app: :strs,
    adapter: Ecto.Adapters.Postgres
end
