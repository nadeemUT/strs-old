defmodule StrsWeb.SessionController do
  use StrsWeb, :controller

  alias Strs.Repo
  alias Strs.Accounts.{User, Auth}

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, session_params) do
    case Auth.login(session_params, Repo) do
      {:ok, user} ->
        conn
        |>  put_session(:current_user, user.id)
        |> put_flash(:info, "Logged in successfully")
        |> redirect(to: "/")
      {:error,_}  ->
        conn
        |> put_flash(:error, "There was problem with your email or password")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> delete_session(:current_user)
    |> put_flash(:info, "Logged out successfully")
    |> redirect(to: "/")
  end
end
