defmodule StrsWeb.Router do
  use StrsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", StrsWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/users", UserController, :index
    resources "/users", UserController

    get "/login", SessionController, :new
    post "/login", SessionContoller, :create
    resources "/session", SessionController, only: [:new, :create]
    get "/sessions", SessionController, :delete
  end

  # Other scopes may use custom stacks.
  # scope "/api", StrsWeb do
  #   pipe_through :api
  # end
end
